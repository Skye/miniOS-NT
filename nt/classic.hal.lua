if not miniOS then error("This program requires miniOS classic", 0) end
if miniOS.NT then printErr("This program requires miniOS classic!") return end

print("HAL: miniOS NT over miniOS classic HAL")

miniOS.NT = { HAL = {}}

miniOS.NT.HAL.uptime = computer.uptime

miniOS.NT.HAL.pullSignal = event.pull

miniOS.NT.HAL.bootAddress = filesystem.drive.toAddress(filesystem.drive.getcurrent())

miniOS.NT.HAL.loadfile = loadfile

print("HAL: preparation complete")

--print("HAL: locating kernel...")

local search
local args = {...}
if (args[1]) then
    print("HAL: user provided kernel location: " .. args[1])
    search = {args[1], args[1] .. ".lua", args[1] .. "/kernel.lua"}
else 
    print("HAL: using default kernel location")
    search = {"kernel.lua", "nt/kernel.lua"}
end
local kernel
--print("HAL: searching for kernel...")
for _,location in pairs(search) do
    if fs.exists(location) and not fs.isDirectory(location) then
        kernel = location
        print("HAL: kernel found at: " .. location)
        break
    end
end
if kernel then
    --print("HAL: loading kernel...")
    local k,e = loadfile(kernel, 'bt', _ENV)
    if k ~= nil then
        print("HAL: kernel loaded")
        --print("HAL: loading modules...")
        miniOS.NT.HAL.modules = {
            --[[keyboard = function()
                --term.clear()
                local counter = 1
                while true do
                    local event = miniOS.NT.kernel.pullEvent(math.huge, "key_down")
                    if event ~= nil then
                        local code = event[3]
                        if code ~= 0 then component.gpu.set(counter, 2, string.char(code))
                        counter = (counter % 80) + 1 end
                    end
                end
            end,--]]
            classic = function()
                local function event_pull (timeout, name, ...)
                    if name == nil then
                        name = timeout
                        timeout = math.huge
                    end
                    local others = {...}
                    local event = miniOS.NT.kernel.pullEvent(timeout, function(event)
                        if type(name) == "string" and type(event[1]) == "string" then
                            if not (event[1]:match(name)) then return false end
                        elseif not name == event[1] then return false end
                        for i,other in ipairs(others) do
                            if event[i + 1] ~= other then return false end
                        end
                        return true
                    end)
                    if event ~= nil then
                        return table.unpack(event)
                    else
                        return
                    end
                end
                event.pull = event_pull
                term.pull = event_pull
                os.sleep = miniOS.NT.kernel.sleep

                _G._OSVERSION = _G._OSVERSION .. " with miniOS NT " .. miniOS.NT.kernel.VERSION
                _G._OSNAME = _G._OSNAME .. " with miniOS NT"
                _G._OSVER = _G._OSVER .. " with NT"

                print("classic: miniOS NT - miniOS classic integration module...")
                print()
                local thread
                local running = {{"command.lua", {"-c"}}, {"command.lua"}}
                local replace = false
                local function onError(th, err, id)
                    if type(err) == "table" and err[1] == "INTERRUPT" then
                        if err[2] == "RUN" then
                            replace = true
                            running[#running + (((#running > 1) and 0) or 1)] = {err[3], {table.unpack(err[4])}}
                        elseif err[2] == "ERR" then
                            printErr("This error is for testing!")
                            return
                        elseif err[2] == "EXIT" then
                        else
                            miniOS.NT.kernel.defaultOnThreadError(th, "Invalid interrupt: " .. err[2], id)
                        end
                    else
                        miniOS.NT.kernel.defaultOnThreadError(th, err, id)
                    end
                end

                while true do
                    if type(running[#running][1]) == "string" then running[#running][1] = loadfile(running[#running][1]) end
                    thread = miniOS.NT.kernel.newThread(running[#running][1], onError)
                    local corou = miniOS.NT.kernel.getThreadCoroutine(thread)
                    local ok, err = coroutine.resume(corou, table.unpack(running[#running][2] or {}))
                    if not ok then onError(corou, err, thread) end
                    miniOS.NT.kernel.waitforend(thread)
                    --print(#running, replace)
                    if #running > 1 and not replace then
                        running[#running] = nil
                        print()
                    elseif #running == 1 then
                        --running[#running+1] = {print}
                        print()
                    end
                    replace = false
                end
            end--[[,
            psychos2 = loadfile('nt/psychos2.mod.lua')--]]
        }
        print("HAL: modules loaded")
        
        --miniOS.NT.HAL.debug = "events"

        print("HAL: starting kernel...")
        local ok, err = pcall(k)
        if not ok then printErr(err) end
        printErr("HAL: Cannot safely exit out of miniOS NT!")
        print("HAL: Press any key to shut down")
        while true do
            local e = computer.pullSignal(math.huge)
            if e == 'key_down' then computer.shutdown() end
        end
    else
        printErr("HAL: kernel did not load. Error Message:\n" .. e)
    end
else
    printErr("HAL: Cannot find miniOS NT kernel!")
end

miniOS.NT = nil
