local io = {}

function io.open(filename, mode)
    return (require 'buffer').new(mode, (require 'filesystem').open(filename, mode))
end

function io.close(file)
    if file == nil then error("not implemented", 1) end
    (require 'buffer').close(file)
end

function io.type(obj)
    if getmetatable(obj) == "file" then
        if not obj.closed then
            return "file"
        else
            return "closed file"
        end
    else
        return nil
    end
end

if miniOS then
    if miniOS.NT then
        return io
    else
        _G.io = io
    end
else
    error("This program requires miniOS classic or miniOS NT", 0)
end