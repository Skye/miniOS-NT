-- TODO: Lua version check!
local psychos2 = {}
for k,v in pairs(_G) do
    psychos2[k] = v
end
psychos2._G = psychos2
psychos2.load = function(ld, source, mode, env)
    return load(ld, source, mode, env or psychos2)
end
psychos2.tprint = print
local enable_dprint = true
psychos2.dprint = function(...)
    if enable_dprint then psychos2.tprint(...) end
end

local psychos2_process
local psychos2_process_interrupt

do
    psychos2.miniOS = setmetatable({}, {__index = miniOS})
    psychos2.miniOS.NT = setmetatable({}, {__index = miniOS.NT})
    psychos2.miniOS.NT.kernel = setmetatable({}, {__index = miniOS.NT.kernel})
    
    local function wrap_yeilding_function(func, ...)
        local running = coroutine.running()
        if running == psychos2_process then
            local args = {...}
            local result
            psychos2_process_interrupt = function() result = table.pack(miniOS.NT.kernel[func](table.unpack(args))) return true end
            coroutine.yield()
            return table.unpack(result)
        elseif miniOS.NT.kernel.getCurrentThread(nil, true) then
            return miniOS.NT.kernel[func](...)
        else
            error("Cannot " .. func .. " in a coroutine that is not a miniOS thread or PsychOS2 process!", 2)
        end
    end
    
    local function wrap_function(func)
        psychos2.miniOS.NT.kernel[func] = function(...) return wrap_yeilding_function(func, ...) end
    end
    
    wrap_function('sleep')
    wrap_function('pullEvent')
    wrap_function('peekEvent')
    wrap_function('waitforend')
end

do
    psychos2.computer = setmetatable({}, {__index = computer})
    function psychos2.computer.getBootAddress()
        return miniOS.NT.HAL.bootAddress
    end
end

do
    local function getMeta(thread_id)
        return miniOS.NT.kernel.getThreadInfo(thread_id or miniOS.NT.kernel.getCurrentThread(), 0).meta
    end

    local function spawnNT(func, name, other_meta)
        local meta = { envars = {} }
        for k,v in pairs(getMeta().envars or {}) do
            meta.envars[k] = v
        end
        for k,v in pairs(other_meta or {}) do
            if k == "envars" then
                for k,v in pairs(getMeta().envars or {}) do
                  meta.envars[k] = v
                end
            else
                meta[k] = v
            end
        end

        return miniOS.NT.kernel.newThread(func, nil, name, meta)
    end
    
    psychos2.os.spawnNT = spawnNT

    function psychos2.os.spawn(func, name)
        return spawnNT(function()
            local corou = coroutine.create(func)
            local new = true
            while coroutine.status(corou) ~= 'dead' do
                local tevent = miniOS.NT.kernel.pullEvent(0)
                new = false
                psychos2_process = corou
                local ok, err = coroutine.resume(corou, table.unpack(tevent or {}))
                psychos2_process = nil                
                if not ok then miniOS.NT.kernel.defaultOnThreadError(corou, err, miniOS.NT.kernel.getCurrentThread()) end
                if psychos2_process_interrupt then new = psychos2_process_interrupt() psychos2_process_interrupt = nil end
            end
        end, name)
    end

    psychos2.os.kill = miniOS.NT.kernel.killThread

    psychos2.os.pid = miniOS.NT.kernel.getCurrentThread

    function psychos2.os.tasks()
        local tasks = miniOS.NT.kernel.getThreadList()
        for k,info in ipairs(tasks) do
            tasks[k] = info.id
        end
        return tasks
    end

    function psychos2.os.taskInfo(thread_id)
        --print(thread_id)
        local info = miniOS.NT.kernel.getThreadInfo(thread_id, 1)
        if info == nil then return nil end
        info.id = nil
        info.parent = ((info.creator or {}).id) or 0
        info.creator = nil
        return info
    end
  
    function psychos2.os.setenv(key, value)
        local meta = getMeta()
        if meta.envars == nil then meta.envars = {} end
        meta.envars[key] = value
    end

    function psychos2.os.getenv(key)
        return (getMeta().envars or {})[key]
    end

    function psychos2.os.setTimeout(timeout)
        -- We do nothing here...
        -- TODO: Maybe simulate the psychOS behavior somehow...
        -- Old behavior: if every psychOS 2 thread has yielded, wait for timeout...
    end

    function psychos2.os.sched() end

    print("psychos2: PsychOS 2 Integration Module")

    local loadfile = loadfile or miniOS.NT.HAL.loadfile
    if loadfile == nil then error("psychos2: No loadfile, cannot proceed.", 0) end

    local config = {}
    loadfile('nt/psychos2.mod.cfg', nil, config)()
    
    psychos2._OSVERSION = "PsychOS " .. tostring(config.version or "2") .. " over miniOS NT " .. miniOS.NT.kernel.VERSION
    
    
    
    
    for _,mod in ipairs(config.core or {}) do
        print("psychos2: loading " .. mod)
        local code, err = loadfile(mod, nil, psychos2)
        if type(code) ~= "function" then error(err) end
        miniOS.NT.kernel.waitforend(psychos2.os.spawn(code, "psychos2 core module " .. mod))
        -- Does a filesystem exist already?
        -- If not, load the psychos2 filesystem library into the miniOS space
        if config.mainfs and (not miniOS.NT.filesystem) and psychos2.fs then
            miniOS.NT.filesystem = psychos2.fs
            miniOS.NT.kernel.pushEvent('filesystem_active')
            miniOS.NT.kernel.sleep(0)
        end
    end

    
    local init = config.init or "module/init.lua"
    if config.init ~= false then
        print("psychos2: starting PsychOS 2 init: " .. init)
        miniOS.NT.kernel.waitforend(psychos2.os.spawn(loadfile(init, nil, psychos2), "psychos2 init from " .. init))
        miniOS.NT.kernel.event_debug.ignore['syslog'] = true
        miniOS.NT.kernel.event_debug.ignore['key_down'] = true
        miniOS.NT.kernel.event_debug.ignore['key_up'] = true
        enable_dprint = false
        _G.print = psychos2.dprint
    else
        print("psychos2: init disabled in config.")
        return --[[
        for _,info in ipairs(miniOS.NT.kernel.getThreadList()) do
            print(info.id, info.name)
        end--]]
    end
end