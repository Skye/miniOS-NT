miniOS.NT.kernel = {}
miniOS.NT.kernel.VERSION = "indev 2"
print("kernel: miniOS NT Kernel " .. miniOS.NT.kernel.VERSION)

local threads = {}
local kernelThread = coroutine.running()
local kernel_sleeping = 0
local events = {}
local event_offset = 0
local lowest_eventposition = 0
local event_limit = 128
local event_waiting = false

local function queueEvent(event)
    events[#events + 1] = event
    print("queueEvent: " .. tostring(#events) .. ' ' .. table.concat(event, ' '))
end

local function dequeueEvent()
    event_offset = event_offset + 1
    print("dequeueEvent: " .. tostring(#events - 1) .. ' ' .. table.concat(events[1], ' '))
    return table.remove(events, 1)
end

local function getEvent(position)
    return events[position]
end

local function getThreadCoroutine(thread_id)
    if thread_id == nil then return nil end
    for corou,info in pairs(threads) do
        if info.id == thread_id then
            return corou
        end
    end
    --print(debug.traceback())
    --error("Thread #" .. tostring(thread_id) .. " does not exist.")
    return nil
end

local currentThread
local function getCurrentThread(corou, strict)
    return (threads[corou or coroutine.running()] or {}).id or ((not strict) and currentThread) or nil
end

local function getThreadName(thread_id)
    return threads[getThreadCoroutine(thread_id)].name
end

local function defaultOnError(corou, err, id)
    local msg = "Error on thread #" .. tostring(id)
    local name = getThreadName(id)
    if name then msg = msg .. " (" .. name .. ")" end
    print(msg)
    if err ~= nil then err = tostring(err) .. "\n" else err = "" end
    print(err .. debug.traceback(corou))
end

local latest_thread_id = 0
local function newThread(code, onerror, name, meta)
    local thread = coroutine.create(code)
    latest_thread_id = latest_thread_id + 1
    threads[thread] = {
        eventposition = 0,
        sleeping = 0,
        eventwaiting = false,
        eventskipping = false,
        onerror = onerror or defaultOnError,
        waitforend = nil,
        id = latest_thread_id,
        name = name,
        creator = threads[getThreadCoroutine(getCurrentThread())],
        meta = meta or {},
    }
    return latest_thread_id
end

local function killThread(thread_id)
    local corou = getThreadCoroutine(thread_id)
    for _,info in pairs(threads) do
        if info.creator and (info.creator.id == thread_id) then
            info.creator = nil
        end
    end
    threads[corou] = nil
end

-- sanitised info only, not full info the kernel uses!
local function getThreadInfo(thread_id, depth, seen)
    if depth ~= nil and depth < 0 then return nil end
    local info
    if type(thread_id) == "table" then
        info = thread_id
    else info = threads[getThreadCoroutine(thread_id)] end
    if info == nil then return nil end
    seen = seen or {}
    for _,seen_info in ipairs(seen) do
        if seen_info.id == info.id then return seen_info end
    end
    local sanitised = {
        name = info.name,
        id = info.id,
        meta = info.meta
    }
    seen[#seen+1] = sanitised
    -- This shouldn't loop, but it might end up doing so if malformed data is passed into it...
    -- So add a safeguard!
    if info.creator then sanitised.creator = getThreadInfo(info.creator, (depth or math.huge) - 1, seen) end

    return sanitised
end

local function getThreadList(depth)
    local list = {}
    for _,info in pairs(threads) do
        list[#list + 1] = getThreadInfo(info, depth)
    end

    return list
end

miniOS.NT.kernel.newThread = newThread
miniOS.NT.kernel.defaultOnThreadError = defaultOnError
miniOS.NT.kernel.getThreadCoroutine = getThreadCoroutine
miniOS.NT.kernel.getThreadName = getThreadName
miniOS.NT.kernel.killThread = killThread
miniOS.NT.kernel.getCurrentThread = getCurrentThread
miniOS.NT.kernel.getThreadInfo = getThreadInfo
miniOS.NT.kernel.getThreadList = getThreadList

local function coroutineCheck(name)
    local current = coroutine.running()
    if not threads[current] then error("Cannot " .. name .. " in a coroutine that isn't a miniOS thread!", 2) end
    return current
end

miniOS.NT.kernel.waitforend = function(target)
    if coroutine.running() == kernelThread then error("Cannot wait for a thread from the kernel!") end
    local current = coroutineCheck('wait for a thread to end')
    threads[current].waitforend = getThreadCoroutine(target)
    miniOS.NT.kernel.sleep(math.huge)
end

local function loadEvents()
    local event
    local uptime = miniOS.NT.HAL.uptime()
    --print(kernel_sleeping)
    -- get all of the events!
    repeat
        event = {miniOS.NT.HAL.pullSignal(0)}
        if (#event > 0) then queueEvent(event)
        else event = nil end
        kernel_sleeping = math.max(kernel_sleeping - (miniOS.NT.HAL.uptime() - uptime), 0)
    until event == nil or kernel_sleeping <= 0
        or #events >= (event_limit - (((event == nil and kernel_sleeping > 0) and 1) or 0))
        or event_waiting
    --print((event == nil), (kernel_sleeping > 0), (not event_waiting))
    --print(event_waiting)
    -- Then try to sleep...
    if event == nil and kernel_sleeping > 0 and not event_waiting then
        event = {miniOS.NT.HAL.pullSignal(kernel_sleeping)}
        kernel_sleeping = math.max(kernel_sleeping - (miniOS.NT.HAL.uptime() - uptime), 0)
        if (#event > 0) then queueEvent(event) end
    end
end

local prev_uptime = miniOS.NT.HAL.uptime()
local function runThreads()
    local uptime = miniOS.NT.HAL.uptime()
    local min_sleep = math.huge
    lowest_eventposition = math.huge
    event_waiting = false
    for thread,info in pairs(threads) do
        if (coroutine.status(thread) == "dead") then
            killThread(info.id)
        else
            currentThread = info.id
            info.eventposition = math.max(0, info.eventposition - event_offset)
            --print(info.eventposition)
            -- TODO: when doing restarting threads, make waitforend still wait even when the coroutine ends!
            if info.sleeping > 0 and not (info.eventwaiting and #events > info.eventposition) and not (info.waitforend ~= nil and (coroutine.status(info.waitforend) == "dead")) then
                --print(info.sleeping, info.eventwaiting, #events, info.eventposition)
                
                info.sleeping = info.sleeping - (uptime - prev_uptime)
                min_sleep = math.min(min_sleep, info.sleeping)
                if (info.eventskipping == true) then info.eventposition = #events end
            else
                min_sleep = 0
                info.sleeping = 0
                repeat
                    local ok, err = coroutine.resume(thread)
                    if not ok then
                        -- TODO: need to double check this doesn't break anything...
                        kernel_sleeping = 0
                        loadEvents()
                        pcall(info.onerror, thread, err, info.id)
                    end
                    --print('loopy', (not info.eventwaiting), (coroutine.status(thread) == "dead"), (info.sleeping > 0 and not (info.eventwaiting and #events > info.eventposition)))
                until (not info.eventwaiting) or (coroutine.status(thread) == "dead") or (info.sleeping > 0 and not (info.eventwaiting and #events > info.eventposition))
            end
            lowest_eventposition = math.min(lowest_eventposition, info.eventposition)
            --print(lowest_eventposition, info.eventposition, event_waiting, min_sleep)
            event_waiting = event_waiting or info.eventwaiting
            --print(info.name, info.eventwaiting)
            currentThread = nil
        end
    end
    lowest_eventposition = math.min(lowest_eventposition, #events)
    event_offset = 0
    kernel_sleeping = min_sleep
    --print(kernel_sleeping)
    prev_uptime = uptime
end

local function cleanEvents()
    local toRemove = math.max(0, #events - event_limit, lowest_eventposition)
    for _=1,toRemove do 
        dequeueEvent()
    end

end

function miniOS.NT.kernel.peekEvent(timeout)
    if (coroutine.running() == kernelThread) then
        error("cannot peek event from kernel")
    end
    local current = coroutineCheck('peek event')
    --print(#events, threads[current].eventposition)
    local slept = false
    threads[current].eventwaiting = true
    if (#events >= threads[current].eventposition) and ((timeout or 0) > 0) then
        slept = true
        print(#events, threads[current].eventposition)
        miniOS.NT.kernel.sleep(timeout, true)
    end
    
    threads[current].eventwaiting = false
    -- TODO: does this work? Note to self: This might break filters... be aware!
    if not slept then coroutine.yield() end
    --if not mark and timeout == math.huge then print("ah!") end
    return getEvent(threads[current].eventposition + 1)
end

function miniOS.NT.kernel.pullEvent(timeout, filter)
    if (coroutine.running() == kernelThread) then
        error("cannot pull event from kernel")
    end
    
    local current = coroutineCheck('pull event')

    --coroutine.yield()

    if timeout == nil then timeout = math.huge end

    local event
    if (filter ~= nil) then
        if type(filter) == "string" then filter = {filter} end
        if type(filter) == "table" then
            local oldfilter = filter
            filter = function(event)
                event = event or {}
                for _,subfilter in ipairs(oldfilter) do
                    if type(subfilter) == "string" and type(event[1]) == "string" then
                        if (event[1]:match(subfilter)) then return true end
                    elseif type(subfilter) == "function" then return subfilter(event[1])
                    else return subfilter == event[1] end
                end
            end
        end
        if type(filter) == "function" then
            local start = miniOS.NT.HAL.uptime()
            repeat
                timeout = timeout - math.min(0, timeout - (miniOS.NT.HAL.uptime() - start))
                event = miniOS.NT.kernel.pullEvent(timeout)
            until filter(event)
        else
            error("Invalid event filter!")
        end
    else
        event = miniOS.NT.kernel.peekEvent(timeout)
        if event ~= nil then
            threads[current].eventposition = threads[current].eventposition + 1
        end
    end

    return event
end

function miniOS.NT.kernel.pushEvent(event, ...)
    while #events >= event_limit do
        miniOS.NT.kernel.sleep(0, true)
    end
    if type(event) ~= 'table' then event = {event, ...} end
    queueEvent(event)
end

function miniOS.NT.kernel.sleep(time, preserveEvents)
    time = time or 0
    if (coroutine.running() ~= kernelThread) then
        local running = coroutineCheck('sleep')
        threads[running].sleeping = time

        local originalskipping = threads[running].eventskipping
        if (preserveEvents ~= true) then
            threads[running].eventposition = #events
            threads[running].eventskipping = true
        end

        coroutine.yield()

        if (preserveEvents ~= true) then
            threads[running].eventskipping = originalskipping
        end

    else
        kernel_sleeping = time
        while (kernel_sleeping > 0) do
            if preserveEvents then loadEvents() end
        end
    end
end

print("kernel: preparation complete")

print("kernel: setting up test code")


local stop = false;
--[[
newThread(function()
    miniOS.NT.kernel.sleep(8)
    stop = true
end)
newThread(function()
    print("Hello World!")
    coroutine.yield()
    miniOS.NT.kernel.sleep(5)
    print("aaaaaaaaaaa!")
    coroutine.yield()
end)
newThread(function()
    coroutine.yield()
    print("Second test here!")
    coroutine.yield()
    miniOS.NT.kernel.sleep(4)
    print("bbbbbbbbbbb!")
    coroutine.yield()
end)



print("kernel: test code preparation complete")
]]

if miniOS.NT.HAL.debug == 'events' then
    miniOS.NT.kernel.event_debug = {ignore = {}}
    newThread(function()
        while true do
            local _, event = pcall(miniOS.NT.kernel.pullEvent, math.huge)
            if not ((miniOS.NT.kernel.event_debug or {}).ignore or {})[event[1]] then
                if (event ~= nil) then pcall(print, "event: " .. tostring(#events) .. " " .. table.concat(event, ' ')) end
            end
        end
    end, nil, "events")
end

if miniOS.NT.HAL.modules then
    --print("kernel: starting modules in threads...")
    for name,mod in pairs(miniOS.NT.HAL.modules) do
        if type(name) == "string" then
            name = "module " .. name
        else
            name = "unnamed module #" .. tostring(name)
        end
        print("kernel: enabling " .. name)
        if type(mod) ~= "function" then print("kernel: attempted to pass non function to load as a module (" .. name .. ") " .. tostring(mod))
        else
            newThread(mod, nil, name)
        end
    end
    miniOS.NT.HAL.modules = {}
end

print("kernel: starting main loop")
while true do
    loadEvents()
    runThreads()
    cleanEvents()
    if stop then break end

end
print("kernel: main loop finished. This is not normal, only for test mode.")
