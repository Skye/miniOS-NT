miniOS = { NT = { HAL = {}} }

local term = {}
do
    do
        local x, y = 1, 1
        local width, height
        local offsetx, offsety = 0, 0

        function term.setCursor(row, col)
            x, y = row, col
        end

        function term.getCursor()
            return x, y
        end
        
        function term.setViewport(w, h, dx, dy, row, col)
            width, height, offsetx, offsety, x, y = w or (width + offsetx - (dx or 0)), h or (height + offsety - (dy or 0)), dx or offsetx, dy or offsety, row or math.min(x, (w or width) - (dx or offsetx)), col or math.min(y, (h or height) - (dy or offsety))
        end
        
        function term.getViewport()
            return width, height, offsetx, offsety, x, y
        end
    end
    do
        local foreground = 0xFFFFFF
        local background = 0x000000
        function term.setForeground(colour)
            foreground = colour
            term.getBound().setForeground(term.getForeground())
        end
        function term.setBackground(colour)
            background = colour
            term.getBound().setBackground(term.getBackground())
        end
        function term.getForeground()
            return foreground
        end
        function term.getBackground()
            return background
        end
        function term.setColour(fg, bg)
            term.setForeground(fg)
            term.setBackground(bg)
            local gpu = term.getBound()
            gpu.setForeground(term.getForeground())
            gpu.setBackground(term.getBackground())
        end
        function term.invertColour()
            term.setColour(term.getBackground(), term.getForeground())

        end
    end
    do
        local bound
        
        function term.getBound()
            return bound
        end
         
        function term.bind(gpu)
            bound = gpu
            term.setViewport(gpu.getResolution())
        end
        
        for address,_ in pairs(component.list("gpu")) do  
          term.bind(component.proxy(address))
          break
        end
    end
    local sync
    function term.write(value)
        if sync then error("Two things are trying to write to the terminal at the same time...") end
        sync = true
        local width, height, offsetx, offsety, x, y = term.getViewport()
        local gpu = term.getBound()
        
        local line = ""
        local iterator = string.gmatch(value, '.')
        repeat
            local character = iterator()

            if character == "\n" or character == nil then
                --gpu.setForeground(term.getForeground())
                --gpu.setBackground(term.getBackground())
                gpu.set(x + offsetx, y + offsety, line)
            else
                line = line .. character
            end
            if character == nil then
                x = x + string.len(line)
                line, iterator = nil
            end
            if character == "\n" then
                line = ""
                x = 1
                y = y + 1
                
                if y > height then
                    gpu.copy(1 + offsetx, 2 + offsety, width, height - 1, 0, -1)
                    y = height
                    term.setCursor(x, y)
                    term.clearLine()
                end
            end
        until iterator == nil or line == nil
        term.setCursor(x, y)
        sync = false
    end
    function term.clearLine()
        local width, height, offsetx, offsety, _, y = term.getViewport()
        local gpu = term.getBound()
        --gpu.setForeground(term.getForeground())
        --gpu.setBackground(term.getBackground())
        gpu.fill(1 + offsetx, y + offsety, width + offsetx, 1, ' ')
        term.setCursor(1, y)
    end
    function term.clear()
        local width, height, offsetx, offsety = term.getViewport()
        local gpu = term.getBound()
        --gpu.setForeground(term.getForeground())
        --gpu.setBackground(term.getBackground())
        gpu.fill(1 + offsetx, 1 + offsety, width + offsetx, height + offsety, ' ')
        term.setCursor(1, 1)
    end
end

local function printProcess(...)
  local args = table.pack(...)
  local argstr = ""
  for i = 1, args.n do
    local arg = tostring(args[i])
    if i > 1 then
      arg = "\t" .. arg
    end
    argstr = argstr .. arg
  end
  return argstr
end
function print(...)
  for address,_ in pairs(component.list("ocemu")) do
    component.invoke(address, "log", ...)
  end
  term.write(printProcess(...) .. "\n", true)
end

term.setViewport(nil, nil, nil, 1, 1, 0)
term.invertColour()
term.clearLine()
term.write("miniOS NT")
term.invertColour()
term.clear()

print("HAL: miniOS NT OpenComputers HAL")

miniOS.NT.HAL.uptime = computer.uptime

miniOS.NT.HAL.pullSignal = computer.pullSignal

function miniOS.NT.HAL.loadfile(name, mode, environment)
  local handle, reason = component.invoke(computer.getBootAddress(), "open", name)
  if not handle then error(reason) end
  local buffer = ""
  repeat
    local data, reason = component.invoke(computer.getBootAddress(), "read", handle, math.huge)
    if not data and reason then error(reason) end
    buffer = buffer .. (data or "")
  until not data
  component.invoke(computer.getBootAddress(), "close", handle)
  -- Tail call!
  return load(buffer, "=" .. name, mode or 'bt', environment or _G)
end
miniOS.NT.HAL.bootAddress = computer.getBootAddress()

local settings = {}
if component.invoke(computer.getBootAddress(), "exists", "nt/oc.hal.cfg") then
    print("HAL: loading settings...")
    miniOS.NT.HAL.loadfile("nt/oc.hal.cfg", nil, settings)()
end

settings.modules = settings.modules or {}
for _,mod in ipairs(settings.modules) do
    local fullmod = 'nt/' .. mod .. '.mod.lua'
    print("HAL: loading module: " .. fullmod)
    miniOS.NT.HAL.modules = miniOS.NT.HAL.modules or {}
    local modfunc, err = miniOS.NT.HAL.loadfile(fullmod)
    if type(modfunc) ~= "function" then error(err) end
    miniOS.NT.HAL.modules[mod] = modfunc
end
miniOS.NT.HAL.debug = settings.debug

--for k,v in pairs(miniOS.NT.HAL.modules) do print(k) end

print("HAL: attempting to load kernel from: nt/kernel.lua")

local kernel, err = miniOS.NT.HAL.loadfile('nt/kernel.lua')

if type(kernel) ~= "function" then error(err) end

return kernel()