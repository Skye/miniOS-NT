-- Attempt to get a filesystem of any kind...
if not miniOS.NT.filesystem then
    print("loader: waiting for filesystem to be active...")
    miniOS.NT.kernel.pullEvent(math.huge, 'filesystem_active')
    print("loader: filesystem active!")
end
