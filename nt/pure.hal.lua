print("HAL: miniOS NT Pure Lua HAL")

miniOS = { NT = { HAL = {}}}

local ticks = {}

local start = os.time()
local last = start
function miniOS.NT.HAL.uptime()
    local current = os.time()

    local uptime = os.difftime(current, start)

    if os.difftime(current, last) >= 1 then
        table.insert(ticks, uptime)
        --print("tick!")
    end
    last = current
    return uptime
end

function miniOS.NT.HAL.pullSignal(timeout)
    timeout = timeout or 0
    -- For the pure Lua HAL, we do nothing except wait...
    local uptime = miniOS.NT.HAL.uptime()
    while ((miniOS.NT.HAL.uptime() - uptime) < timeout) and (#ticks <= 0) do end
    if (#ticks > 0) then
        local tick = table.remove(ticks, 1)
        local tock
        if tick % 2 == 0 then
            tock =  "tick"
        else
            tock = "tock"
        end
        return tock, tick
    else return end
end

print("HAL: preparation complete")

print("HAL: starting kernel...")
loadfile("kernel.lua", 'bt', _G)()
