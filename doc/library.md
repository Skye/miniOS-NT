miniOS NT - Library Specification
=================================

Version: `indev.3`  
This is subject to change! It is possible that some changes will be **incompatible**. Incompatibility between versions of this specification will be mentioned here when they happen.

## Introduction
One of the main problems in most OSes for OpenComputers is that libraries, once loaded, are never unloaded. The behaviour is the exact same as Lua. This works most of the time, but does mean libraries that are used once and never used again hang around and wastes memory.  
It is not possible to safely unload most libraries since they were written under the assumption that they would not be unloaded. Some libraries should only be partially unloaded. Sometimes state needs to be preserved. The miniOS NT library format is designed to facilitate this.  
Some other quality of life features, like configuration files and exporting multiple libraries in one go are also intended to be in future version of this specification.

## VLEX Support
Support for [VLEX](https://github.com/Adorable-Catgirl/Random-OC-Docs/blob/master/formats/velx/v1.md) is intended, but not covered by this version of the specification.

## File Name
A miniOS NT library needs to end in `.lib.lua` to indicate that it is a library, and that it is written in Lua.
The rest of the file name, will be considered the library name, and should only be formed of the characters `A`-`Z`, `0`-`9`, `_`, `-` and `+`.

## Library Name
The library name should only be formed of the characters `A`-`Z`, `0`-`9`, `_`, `-` and `+`. This will be the name of the library

## Using Libraries
This specification does not control how libraries will be used by programs. It is up to the OS provide things like `require`.

## Library Loading
The library will be loaded and executed. It will be passed parameters with varags (similar to a program).

### Parameters
1st Parameter - `nil` or a value returned by this library before.

### Return Values
1st Return Value - The value returned by libraries as in standard Lua libraries. Typically a table. May be cached temporarily. May be discarded at any moment. This is the thing that would be given to a program that used `require`.  
2nd Return Value - This value is saved and not discarded (unless the user wants it to be). This will hold the "state" of the library. This is passed to the library as the 1st parameter on execution if the 1st return value is cleared, to allow the library to restore its state.

## Implementation Notes
* Unloading should be able to be done automatically with Lua weak references in tables.
* The implementation doesn't have to unload.
  * If the implementation doesn't unload, it doesn't need to store the "state" value.
  * The implementation has to unload to actually be memory saving!
    * If the implementation unloads, it has to store the "state" value.
* It should be assumed that the library can be unloaded at any time for any reason and that all state not stored in the "state" value will be lost at any moment.
  * Some libraries will need to be rewritten
* Some basic libraries without state will work just fine in this format without any changes, or only minimal changes.

## Unresolved areas
These are things that require more consideration on if they belong in this specification:
* Configuration
* Multiple libraries in one file
* [VLEX support](#vlex-support)
* Seperate state for different programs / users
* Which libaries / APIs will be availible?
  * Supporting OSes other than miniOS NT?
