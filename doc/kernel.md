# miniOS NT - Kernel API
Version: `indev.2` (draft)  
Every function documented here is within `miniOS.NT.kernel`.

This includes features that are not implemented yet!

## Event System
TODO: Explain how events work. also, need to explain how coroutine interactions will mess with this.

**Warning**: If you call these functions from outside a miniOS NT thread, they may crash.

### `peekEvent`(`timeout`: number): event table or `nil`
Used to see what the next event pulled by a call to `pullEvent` will be.  
It will wait for an event up to the timeout if provided. If no timeout is provided, timeout is assumed to be 0.  
There is no filter, unlike `pullEvent`.  
Will return `nil` when there is no event already queued or queued within the timeout.

### `pullEvent`(`timeout`: number, `filter`: function or string or list): event table or `nil`
Used to get and dequeue an event from the (effectively per-thread) queue.  
It will wait for an event up to the timeout if provided. If no timeout is provided, timeout is assumed to be 0.  
Will return `nil` when there is no matching event already queued or queued within the timeout.  
The filter is used to only select the events the thread cares about.
#### Filter String
If the filter is a string, it will be compared against the first component of the event, using `string.gmatch`. The first event that matches will be returned. Unmatched events before that will be discarded.
#### Filter List
If the filter is a table, it will assume it is a list. The first component of the event will be directly compared against each item in the list. If the component is equal to an item in the list, it will be accepted.
#### Filter Function
If the filter is a function, the function will be used to check every event.  
The event will be passed to the function as its first parameter.  
If the filter returns true, then `pullEvent` will return the event.  
The remaining time from the timeout will be passed as the second parameter to the filter.  

It is allowed to use the fetched events with the filter. In effect this allows you to use `pullEvent` with the filter as a "callback" function.
##### Example Filter
```Lua
function filter(event_to_test, time_remaining)
    if thing_we_want(event_to_test) then
        -- We found it!
        return true
    else
        -- Keep Looking
        return false
    end
end
```

### `pushEvent`(`event`: event table)
Used to push an event to the back of the event queue so that other threads can get the event. If the event queue is full, this will make the thread sleep until the event queue has room.

### `sleep`(`timeout`: number, `keep_events`: boolean)
Used to make a thread sleep.  The `timeout` is in seconds.  
If `keep_events` is `false` or `nil`, then any events the thread would have otherwise received will be discarded.

### `waitforend`(`threadid`: number, `ignore_restart`: boolean or nil)
The thread calling this will sleep until the thread with the ID given in `threadid` has finished. If `ignore_restart` is `false` or `nil` then this will return when the thread restarts. If `ignore_restart` is `true`, it will only return when the thread does not restart.

## Thread Management
Threads are coroutines that are managed by the kernel.
* TODO: Add extra bits to the API

### `newThread`(`code`: function, `onerror`: function, `name`: string or nil, `meta`: table or nil, `restarts`: boolean or nil, `start_sleep`: number or nil, `onresume`: function or table or nil): number
Creates a new thread. `code` is used to create the coroutine that is used in the thread. `name` is used to name the thread. `meta` becomes the table used to store optional (not used by the kernel) infomation with the thread.  
If `restarts` is `true`, `code` will be saved to be used to restart the thread if it finishes.  
If `start_sleep` is a number greater than `0`, then the thread will be sleeping until `start_sleep` seconds. 

If `onresume` is a table, it will be treated as a stack; each time the coroutine of the thread is resumed, the top value of the table is removed, unpacked, and then passed to the coroutine, nutil the table is empty.  
If `onresume` is a function, it will be called every time the thread is resumed, and the results will be passed to the coroutine. While this code will run in the kernel coroutine, errors will be silently ignored, and a special case is added to allow for pulling of events in this state, but only with a timeout of 0.

### `defaultOnThreadError`(`corou`: coroutine, `err`: string, `id`: number)
The default error handler for when a thread crashes. This is accessable to external programs to let custom error handlers call the default error handler if needed.  
This also is the format of an error handler for the `onerror` parameter of `newThread(code, onerror, name)` function.

### `getThreadCoroutine`(`thread_id`: number): coroutine or `nil`
Given a thread ID, returns the current coroutine of that thread, if that thread exists. It will lead to unexpected behaviour if you try to resume the coroutine given under certain conditions.

### `getThreadName`(`thread_id`: number): string or `nil`
Given a thread ID, returns the name of that thread, if that thread exists and has a name.

### `killThread`(`thread_id`: number)
Given a thread ID, removes that thread from the scheduler. Does nothing if the thread doesn't exist.

### `getCurrentThread`(`corou`: coroutine or nil, `strict`: boolean or nil): number or `nil`
Attempt to get the thread ID of the currently running thread, determined based on `corou` or the currently running coroutine.  
If neither of them are threads, and `strict` is `nil` or `false`, then it will get the thread ID of the currently running thread according to the current state of the scheduler.

### `getThreadInfo`(`thread_id`: number, `depth`: number or nil): table
Get information about a thread, given a thread ID.  
Returns a table with the `name`, `id`, `timeout`, `restarts`, `sleeping`, and the table of `meta` information of the thread.  
Also returns the info of the `parent` of the thread, which is the thread that created it. This is recursive. Use `depth` to control how deeply it recurses. If it is `0` it will not get the info of the parent thread, if it is `1` it will, if it is `2` it will get the information of the parent, and it's parent, etc...  
If `restarts` is true, the thread will restart when it ends. `sleeping` is how many seconds the thread has left to sleep.

### `getThreadList`(`depth`: number): table/list
Get a list of all the threads running. Each entry in the list is the same as what you could get with `getThreadInfo(...)`, so `depth` is used to control how many levels of recursion the `parent` information is.

### `setThreadTimeout`(`thread_id`: number, `timeout`: number or nil)
Set the "timeout" of the thread. This is how long until the thread is automatically woken up from yielding (if it is not sleeping or waiting for an event). By default `timeout` is `math.huge`. If timeout is nil, it is set to `math.huge`.  
To get the timeout, use [`getThreadInfo(thread_id, 0).timeout`](#getthreadinfothreadid-number-depth-number-or-nil-table).  
Timeouts are not copied when making new threads.

### `setThreadRestart`(`thread_id`: number, `code`: function or nil)
Enable / Disable a thread from restarting if it ends.  
If `code` is `nil` then it will stop the thread from restarting.
If `code` is a function, then that function will be used to restart the thread.

## Debugging
TODO: Explain debugging helpers

## Other
TODO: other stuff?

## HAL Interaction
TODO: explain how this interacts with the HAL
